import numpy as np
import csv
from datetime import datetime
import matplotlib.pyplot as plt


def readCsv(filename):
    """ Read CSV the hardway, not using library """
    with open(filename, 'rb') as csv_file:
        reader = csv.reader(csv_file, delimiter=',')
        x = list(reader)
        return np.array(x[1:]).astype("string")


def parseDateTime(date_column, unit):
    """ parse time into POSIX, week number...etc. """
    m_date_column = []
    for i, date in enumerate(date_column):
        date_time = datetime.strptime(date, "%m/%d/%Y %H:%M")
        m_date_column.append(date_time)
    if unit == "POSIX":
        return m_date_column
    elif unit == "day":
        days = []
        for i, date in enumerate(m_date_column):
            days.append(datetime.isocalendar(m_date_column[i])[2])
        return days
    elif unit == "week":
        weeks = []
        for i, date in enumerate(m_date_column):
            weeks.append(datetime.isocalendar(m_date_column[i])[1])
        return weeks
    else:
        raise ValueError("Wrong unit format")


def plotEnergyConsumption(raw_data, week_number):
    print("Saving Energy consuption ISO week number {}".format(week_number))
    POSIX = parseDateTime(raw_data[:, 0], "POSIX")
    weeks = parseDateTime(raw_data[:, 0], "week")
    count = 0
    last_i = 0
    for i in range(len(weeks)):
        if weeks[i] == week_number:
            count += 1
            last_i = i
    appliances = raw_data[:, 1].astype("int")
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10), dpi=200)
    ax1.plot(POSIX, appliances)
    ax1.set_title("Appliances energy consumption for whole period")
    ax2.plot(POSIX[last_i - count + 1:last_i + 1],
             appliances[last_i - count + 1:last_i + 1])
    ax2.set_title(
        "Appliances energy consumption in ISO week number {} ".format(week_number))
    plt.savefig("EnergyConsumption.png")


def heatMapEnergyConsumption(raw_data, week_number):
    print("Saving heat map of Energy consuption ISO week number {}".format(week_number))
    POSIX = parseDateTime(raw_data[:, 0], "POSIX")
    weeks = parseDateTime(raw_data[:, 0], "week")
    count = 0
    last_i = 0
    for i in range(len(weeks)):
        if weeks[i] == week_number:
            count += 1
            last_i = i
    heat_map = np.zeros((7, 24), dtype=float)
    count_2 = last_i - count + 1
    for i in range(7):
        for j in range(24):
            for k in range(6):
                heat_map[i, j] += float(raw_data[count_2, 1])
                count_2 += 1
    plt.figure(figsize=(10, 3), dpi=200)
    # plt.matshow(heat_map, origin="lower")
    plt.imshow(heat_map, cmap='YlOrRd',
               interpolation='nearest', origin='lower')
    plt.title(
        "Heat map of Energy Consumption hourly per week {}".format(week_number))
    plt.ylabel("Day of week")
    plt.xlabel("Hour of day")
    plt.colorbar()
    plt.savefig("heatMapEnergyConsumption.png")
    # days = parseDateTime(raw_data[last_i - count + 1: last_i + 1, 0], "day")
    # print len(days)


def histogramAppliances(raw_data):
    appliances = raw_data[:, 1].astype("int")
    plt.figure(figsize=(10, 5), dpi=200)
    plt.hist(appliances, bins='auto')

    plt.title(
        "Histogram of Energy Consumption by appliances")
    plt.xlabel("Appliances Wh")
    plt.ylabel("Frequency")
    plt.savefig("histogramAppliances.png")


def energyNSMplot(raw_data):
    energy = raw_data[:, 1].astype('float')
    POSIX = parseDateTime(raw_data[:, 0], "POSIX")
    nsm = np.zeros_like(POSIX, dtype='int')
    for i in range(len(POSIX)):
        nsm[i] = POSIX[i].hour * 3600 + POSIX[i].minute * 60 + POSIX[i].second

    plt.figure(figsize=(10, 5), dpi=200)
    print("Correlation coefficient energy vs NSM: {}".format(
        np.corrcoef(nsm, energy)[0, 1]))
    plt.scatter(nsm, energy)
    plt.title(
        "Appliances energy consumption vs NSM")
    plt.xlabel("Number of second from midnight")
    plt.ylabel("Appliances Wh")
    plt.savefig("NSMplot.png")


def energyVsPressHg(raw_data):
    energy = raw_data[:, 1].astype('float')
    press_hg = raw_data[:, 22].astype('float')

    plt.figure(figsize=(10, 5), dpi=200)
    print("Correlation coefficient energy vs mmHg: {}".format(
        np.corrcoef(press_hg, energy)[0, 1]))
    plt.scatter(press_hg, energy)
    plt.title(
        "Appliances energy consumption vs Pressure (mmHg)")
    plt.xlabel("Milimeter of Mercury")
    plt.ylabel("Appliances Wh")
    plt.savefig("energyVsMercury.png")


raw_data = readCsv('./energydata_complete.csv')
# Choose a ISO week number(range from 2 to 21)
plotEnergyConsumption(raw_data, week_number=3)
heatMapEnergyConsumption(raw_data, week_number=3)
histogramAppliances(raw_data)
energyNSMplot(raw_data)
energyVsPressHg(raw_data)
