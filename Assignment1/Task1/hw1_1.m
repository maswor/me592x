%% code to read and generate histogram of image pixel intensities
clc;
close all;
clear all;

X = rgb2gray(imread('sudoku-original.png'));
figure; 
histogram(X);
xlabel('pixel intensity')
ylabel('number of pixels')
title('Histogram of pixel intensities in Sudoku image')