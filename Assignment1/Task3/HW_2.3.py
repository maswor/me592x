# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 16:22:14 2018

@author: Tryambak
"""

import pandas
import numpy as np
import statistics
import scipy
from scipy import stats
dataset = np.loadtxt( 'airfoil_self_noise.dat' )

#Inputs
frequency=dataset[:,0]  #Frequency(Hz)
angle=dataset[:,1] #Angle of attack in degrees 
ch_length=dataset[:,2] #Chord Length (in meters)
velocity=dataset[:,3]  #Free-stream velocity in meters per second 
thickness=dataset[:,4] #Suction side displacement thickness (in meters)

#Output
output=dataset[:,5] #Scaled sound pressure level (in decibels)

#Results
results=np.zeros([6,6])
no_columns=dataset.shape[1]
i=0
while i < no_columns:
    data=dataset[:,i]
    results[0,i]=statistics.mean(data)  #Mean 
    results[1,i]=statistics.pvariance(data)  #Variance 
    results[2,i]=statistics.median(data)  #Median
    
    results[3,i]=scipy.stats.kurtosis(data, fisher=True, bias=True)  #Kurtosis
    #If Fisher’s definition is used, then 3.0 is subtracted from the result to give 0.0 for a normal distribution
    #If bias is False then the kurtosis is calculated using k statistics to eliminate bias comming from biased moment estimators
    
    results[4,i]=scipy.stats.skew(data, bias=True)  #Skewness
    #If bias is False,then the calculations are corrected for statistical bias
    
    results[5,i]=np.ptp(data)  #Range
    i += 1

results_final=pandas.DataFrame(results)   

# manually specify column names
results_final.columns = ['Frequency', 'Angle of attack', 'Chord Length', 'Free-stream velocity', 
                   'Displacement Thickness', 'Sound Pressure-Level']

# manually specify row names
results_final.index = ['Mean', 'Variance', 'Median', 'Kurtosis', 
                   'Skewness', 'Range']

Results = pandas.DataFrame(results_final).to_csv('Results_2.3.csv')